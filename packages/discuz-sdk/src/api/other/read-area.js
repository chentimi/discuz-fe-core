import { handleError } from '../utils/handle-error';
import request from '../_example';
const validateRules = {
  typeid: {
    // 地区省市区数据格式
    type: 'number',
    required: false,
  },
};
/**
 * 描述：获取地区省市信息
 * 请求方式：GET
 * @param {axios config} opt 请求配置信息
 * @returns {promise}
 */
export async function readArea(opt = {}) {
  try {
    const { params = {}, data = {}, ...others } = opt;
    const options = {
      url: '/api/v3/area', // 请求地址
      method: 'GET',
      params,
      data,
      ...others,
      validateRules,
    };
    const result = await request.dispatcher(options);
    return result;
  } catch (error) {
    return handleError(error);
  }
}
