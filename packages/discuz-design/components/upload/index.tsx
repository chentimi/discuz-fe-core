import React from 'react';
import { baseComponentFactory } from '../../extends/baseComponent';
import { UploadViewAdapter } from './layouts/index';
import { UploadLogicalAdapter } from './adapters/index';
import { UploadProps} from './interface';

interface UploadState {}

interface UploadLayoutProps {}

interface UploadAdapter {}

export default class Upload extends baseComponentFactory<
  UploadProps,
  UploadState,
  UploadLayoutProps,
  UploadAdapter
>({
  viewAdapter: UploadViewAdapter,
  logicalAdapter: UploadLogicalAdapter,
}) {
  constructor(props) {
    super(props);
    this.uploadRef = React.createRef();
  }

  static defaultProps = {};
  trggerInput = () => {
    this.uploadRef.current.trggerInput();
  };
  render() {
    const { RenderComponent } = this;
    
    return <RenderComponent  onRef={this.uploadRef} {...this.props}>{this.props.children}</RenderComponent>;
  }
}
